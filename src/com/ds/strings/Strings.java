package com.ds.strings;

public class Strings {

    public static void main(String[] args) {
        System.out.println(Substring.isSubstring(args[0], args[1]) ? "A substring" : "Not a substring");
    }
}
