package com.ds.strings;

public class Substring {

    private Substring() {}

    static boolean isSubstring(String text, String substringText) {
        if (substringText.length() == 0 || substringText.equals("*")) {
            return true;
        }

        char[] string = text.toCharArray();
        char[] substring = substringText.toCharArray();

        boolean startsWithWildcard = substring[0] == '*';

        int firstCharIndex = (substring.length > 1 && substring[0] == '\\' && substring[1] == '*') ? 1 : 0;

        int i = 0;
        while (i < string.length) {
            if (string[i++] == substring[firstCharIndex] || startsWithWildcard) {

                if (substring.length - 1 == firstCharIndex) return true;

                int j = firstCharIndex + 1;
                while (j < substring.length) {

                    // Star character
                    if (j < substring.length - 1 && substring[j] == '\\' && substring[j + 1] == '*') {
                        j++;
                    }

                    // Wildcard
                    if (startsWithWildcard && j == 1 || substring[j - 1] != '\\' && substring[j] == '*') {
                        while (j < substring.length - 1 && substring[j + 1] == '*') {
                            j++;
                        }

                        // Star at last place of string
                        if (i == string.length - 1 || j == substring.length - 1) {
                            return true;
                        }

                        // If we search for star right after wildcard
                        if (j + 2 < substring.length && substring[j + 1] == '\\' && substring[j + 2] == '*') {
                            j++;
                        }

                        // Loop until char mismatch or char after wildcard found
                        while (i < string.length - 1 && string[i] != substring[j + 1]) {
                            i++;
                        }

                        if (startsWithWildcard) {
                            i++;
                        }
                        j++;
                    } else if (string[i] != substring[j]) {
                        // Char mismatch
                        break;
                    } else {
                        i++;
                        j++;
                    }

                    // Substring found
                    if (substring.length - 1 < j) {
                        return true;
                    }

                    // String end
                    if (string.length - 1 < i) {
                        break;
                    }
                }
                i++;
            }
        }

        return false;
    }
}
