package com.ds.strings;

import java.util.List;

public class SubstringTest {

    public static void main(String[] args) {
        doesNotAcceptEmptyString();
        doesNotAcceptEmptySubstring();

        isSubstring();
        notASubstring();

        isSubstringOnSecondFirstCharOccurence();

        isSubstringIfWildcard();
        notASubstringIfCharAfterWildcardNotFound();

        isSubstringIfWildcardOnLastPlace();
        isSubstringIfWildcardOnFirstPlace();

        isSubstringTwoWildcards();

        isSubstringStarCharOnFirstPlace();
        isSubstringStarInTheMiddle();
        isSubstringStarAtTheEnd();

        isSubstringStarAndWildcard();

        isSubstringComplexExample();

        List.of(
                List.of("a*bcd\\", "*"),
                List.of("a*bcd\\", "a*d\\"),
                List.of("a*bcd\\", "cd\\"),
                List.of("", "*"),
                List.of("", ""),
                List.of("\\", "\\"),
                List.of("\\\\", "\\"),
                List.of("*", "*"),
                List.of("*", "\\*"),
                List.of(" ", " "),
                List.of(" ", ""),
                List.of("dynamic", "***"),
                List.of("dynamic", ""),
                List.of("123", "*"),
                List.of("123", "*******"),
                List.of("123*45", "1*\\*4**"),
                List.of("\\\\\\", "\\"),
                List.of("\\\\\\", "*"),
//                List.of("it is very long long long long long long long test", "it * test"),
                List.of("*1", "\\*"),
                List.of("*1", "\\**"),
//                List.of("*1", "\\*1*"),
                List.of("!@# !@# #@!", "*"),
                List.of("!@#/#@!", "*/**"),
                List.of("//*/\\", "/*/\\"),
//                List.of("a * rr !", "a \\** !"),
                List.of(" arr ", " "),
                List.of("***", "*"),
                List.of("***", "\\*\\*\\*"),
                List.of("*** ", "\\**\\*\\*"),
                List.of("zAq!@wSXxq\\", "q!*q\\"),
                List.of("q!@wSXxq\\", "q!*q\\"),
                List.of("a*bcd\\", "a*d\\"),
                List.of("a*bcd\\", "cd\\"),
                List.of("", "*"),
                List.of("\\", "\\"),
                List.of("\\\\", "\\")
        ).forEach(l -> isSubstring(l.get(0), l.get(1)));

        List.of(
                List.of("a*bcd\\", "\\*d"),
                List.of("lorem ipsum", "ipsum "),
                List.of(" lipsum", "* * lipsum*"),
                List.of("\\lorem ipsum *", "* m \\*"),
                List.of("\\lorem ipsum *", "* m\\*"),
                List.of(" dy\\*nami c", "*dy*\\*namic"),
                List.of("123", "\\*"),
                List.of("123", "\\**"),
                List.of("\\\\\\", "\\*"),
                List.of("*1", "\\*1\\*"),
//                List.of("!@# $#@!@$# #", "* $*\\t"),
                List.of(" * @", "\\**@\\*"),
                List.of("a*bcd\\", "\\*d")
        ).forEach(l -> isNotSubstring(l.get(0), l.get(1)));
    }

    static void isSubstring(String string, String substring) {
        assert Substring.isSubstring(string, substring);
    }

    static void isNotSubstring(String string, String substring) {
        assert !Substring.isSubstring(string, substring);
    }

    static void doesNotAcceptEmptyString() {
        try {
            Substring.isSubstring("", "efg");
        } catch (AssertionError e) {
            assert true;
        }
    }

    static void doesNotAcceptEmptySubstring() {
        assert Substring.isSubstring("abcdefgh", "");
    }

    static void isSubstring() {
        assert Substring.isSubstring("abcdefgh", "efg");
    }

    static void notASubstring() {
        assert !Substring.isSubstring("abcdefgh", "afg");
    }

    static void isSubstringOnSecondFirstCharOccurence() {
        assert Substring.isSubstring("aedgbcdefgh", "efg");
    }

    static void isSubstringIfWildcard() {
        assert Substring.isSubstring("abcdefgh", "e*h");
    }

    static void notASubstringIfCharAfterWildcardNotFound() {
        assert !Substring.isSubstring("abcdefgh", "a*s");
    }

    static void isSubstringIfWildcardOnLastPlace() {
        assert Substring.isSubstring("abcdefgh", "bcd*");
    }

    static void isSubstringIfWildcardOnFirstPlace() {
        assert Substring.isSubstring("abcdefgh", "*def");
    }

    static void isSubstringTwoWildcards() {
        assert Substring.isSubstring("abcdefgh", "a*d*h");
    }

    static void isSubstringStarCharOnFirstPlace() {
        assert Substring.isSubstring("abc*defgh", "\\*def");
    }

    static void isSubstringStarInTheMiddle() {
        assert Substring.isSubstring("abcde*fgh", "de\\*f");
    }

    static void isSubstringStarAtTheEnd() {
        assert Substring.isSubstring("abcdef*gh", "def\\*");
    }

    static void isSubstringStarAndWildcard() {
        assert Substring.isSubstring("abcdef*gh", "a*f\\*gh");
    }

    static void isSubstringComplexExample() {
        assert Substring.isSubstring("asdfhwoeifnkghfdogow**/*dkjhf*k;hag/", "dfhw*hfd*w\\*\\*/*\\**/");
    }
}
